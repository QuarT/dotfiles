#!/bin/bash
# depend on `bash-preexec.bash`

CMD_MAX_EXEC_TIME=5

# source bash_preexec.bash
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source ${SCRIPT_DIR:?"$0 in wrong place"}/bash-preexec.sh


# derived from:
#
# https://gist.github.com/mkottman/1936195
# A two-line colored Bash prompt (PS1) with Git branch and a line decoration
# which adjusts automatically to the width of the terminal.
# Recognizes and shows Git, SVN and Fossil branch/revision.
# Screenshot: http://img194.imageshack.us/img194/2154/twolineprompt.png
# Michal Kottman, 2012

RESET="\[\033[0m\]"
RED="\[\033[0;31m\]"
GREEN="\[\033[01;32m\]"
BLUE="\[\033[01;34m\]"
MAGENTA="\[\033[01;35m\]"
YELLOW="\[\033[0;33m\]"

PROMPT_SYMBOL=${PROMPT_SYMBOL:-❯}

PS_LINE=`printf -- '- %.0s' {1..200}`

function parse_git_branch {
  PS_BRANCH=''
  PS_FILL=${PS_LINE:0:$COLUMNS}
  if [ -d .svn ]; then
    PS_BRANCH="(svn r$(svn info|awk '/Revision/{print $2}'))"
    return
  elif [ -f _FOSSIL_ -o -f .fslckout ]; then
    PS_BRANCH="(fossil $(fossil status|awk '/tags/{print $2}')) "
    return
  fi
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  PS_BRANCH="(git ${ref#refs/heads/}) "
}

# add wall time time of lastcommand
function timer_start {
  typeset -g '__prev_cmd_timer'="${__prev_cmd_timer:-$SECONDS}"
}

function timer_stop {
  if [ -z "$__prev_cmd_timer" ]; then
    __prev_cmd_timer_show=""
  else
    __prev_cmd_timer_show=$(($SECONDS - $__prev_cmd_timer))
  fi
  unset __prev_cmd_timer
}

function prompt_command_func {
  parse_git_branch
  timer_stop
}

preexec_functions+=(timer_start)
precmd_functions+=(prompt_command_func)

PS_INFO="$GREEN\u@\h$RESET:$BLUE\w"
PS_GIT="$YELLOW\$PS_BRANCH"

# adapted from
# 165392 => 1d 21h 56m 32s
# https://github.com/sindresorhus/pretty-time-zsh
function prompt_human_time_to_var {
  local human total_seconds=$1 var=$2
  local days=$(( total_seconds / 60 / 60 / 24 ))
  local hours=$(( total_seconds / 60 / 60 % 24 ))
  local minutes=$(( total_seconds / 60 % 60 ))
  local seconds=$(( total_seconds % 60 ))
  (( days > 0 )) && human+="${days}d "
  (( hours > 0 )) && human+="${hours}h "
  (( minutes > 0 )) && human+="${minutes}m "
  human+="${seconds}s"

  # store human readable time in variable as specified by caller
  typeset -g "${var}"="${human}"
}

function calc_exec_time {
  if [ ${__prev_cmd_timer_show:-0} -gt $CMD_MAX_EXEC_TIME ]; then
    prompt_human_time_to_var ${__prev_cmd_timer_show} "__last_exec_time"
  else
    __last_exec_time=""
  fi
  echo ${__last_exec_time}
}

#export PS1="\[\033[0G\]${PS_INFO} ${PS_GIT}  ${YELLOW}\$(calc_exec_time)${RESET}\n${MAGENTA}${PROMPT_SYMBOL}${RESET} "

#`ESC[0g` (Clear horizontal tab) conflicts with conda env prompt
export PS1="${PS_INFO} ${PS_GIT}  ${YELLOW}\$(calc_exec_time)${RESET}\n${MAGENTA}${PROMPT_SYMBOL}${RESET} "

